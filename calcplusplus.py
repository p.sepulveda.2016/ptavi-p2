#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import calcoohija
import csv


if __name__ == "__main__":

    try:
        with open(sys.argv[1], 'r') as fichero:
            lista = list(csv.reader(fichero, delimiter=','))
            for linea in lista:
                op1 = linea[1]
                for numero in linea[2:]:
                    c = calcoohija.CalculadoraHija(float(op1), float(numero))
                    if linea[0] == "suma":
                        op1 = c.plus()
                        resultado = op1
                    elif linea[0] == "resta":
                        op1 = c.minus()
                        resultado = op1
                    elif linea[0] == "multiplica":
                        op1 = c.multi()
                        resultado = op1
                    elif linea[0] == "divide":
                        try:
                            op1 = c.div()
                            resultado = op1
                        except ZeroDivisionError:
                            resultado = "Error. No puedes dividir entre 0."
                    else:
                        sys.exit("Suma, resta, multiplica o divide.")
                print(resultado)
    except ValueError:
        resultado = "Error. Introduce parametros numericos"
    fichero.close()
