#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import calcoohija


if __name__ == "__main__":

    f = open(sys.argv[1], 'r')
    contenido = f.readlines()
    try:
        for linea in contenido:
            line = linea.rstrip('\n')
            lista = line.split(",")
            op1 = lista[1]
            for numero in lista[2:]:
                c = calcoohija.CalculadoraHija(float(op1), float(numero))
                if lista[0] == "suma":
                    op1 = c.plus()
                    resultado = op1
                elif lista[0] == "resta":
                    op1 = c.minus()
                    resultado = op1
                elif lista[0] == "multiplica":
                    op1 = c.multi()
                    resultado = op1
                elif lista[0] == "divide":
                    try:
                        op1 = c.div()
                        resultado = op1
                    except ZeroDivisionError:
                        resultado = "Error. No puedes dividir entre 0."
                else:
                    sys.exit("Unicamente suma, resta, multiplica o divide.")
            print(resultado)
    except ValueError:
        sys.exit("Error. Introduce parametros numericos")
    f.close()
