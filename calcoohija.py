#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    def multi(self):
        """ Funcion para multiplicar dos cifras. Deben ser float. """
        return self.num1 * self.num2

    def div(self):
        """ Funcion para dividir dos cifras. """
        try:
            return self.num1/self.num2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed.")


if __name__ == "__main__":

    try:
        NUMERO1 = float(sys.argv[1])
        NUMERO2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: No has introducido parametros numericos.")

    calculadora = CalculadoraHija(NUMERO1, NUMERO2)
    if sys.argv[2] == "suma":
        resultado = calculadora.plus()
    elif sys.argv[2] == "resta":
        resultado = calculadora.minus()
    elif sys.argv[2] == "multiplica":
        resultado = calculadora.multi()
    elif sys.argv[2] == "divide":
        resultado = calculadora.div()
    else:
        sys.exit('Operación sólo puede ser suma, resta, multiplica o divide.')

    print(resultado)
