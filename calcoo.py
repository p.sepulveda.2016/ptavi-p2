#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys


class Calculadora():
    def __init__(self, num1, num2):
        "Esto es el método iniciliazador"
        self.num1 = num1
        self.num2 = num2

    def plus(self):
        """ Funcion para sumar dos cifras. Deben ser float. """
        return self.num1 + self.num2

    def minus(self):
        """ Funcion para restar dos cifras. """
        return self.num1 - self.num2


if __name__ == "__main__":

    try:
        NUMERO1 = float(sys.argv[1])
        NUMERO2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: No has introducido parametros numericos.")

    calculator = Calculadora(NUMERO1, NUMERO2)
    if sys.argv[2] == "suma":
        resultado = calculator.plus()
    elif sys.argv[2] == "resta":
        resultado = calculator.minus()
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(resultado)
